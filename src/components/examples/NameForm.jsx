import React, { Component } from 'react';
import { render } from 'react-dom';

export default class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    const formData = {};
    formData["phone"] = this.refs["phone"].value;
    formData["email"] = this.refs["email"].value;
    console.log('-->', formData);
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input ref="phone" className="phone" type='tel' name="phone"/>
          <input ref="email" className="email" type='tel' name="email"/>
          <input type="submit" value="Submit"/>
        </form>
      </div>
    );
  }
}