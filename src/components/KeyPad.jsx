'use strict';

import React, { Component } from 'react'
import { render } from 'react-dom'
import { connect } from 'react-redux'
import Key from './Key.jsx'

export default class KeyPad extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        {this.props.keys.map((value, i) => {
          return <Key key={`k${value}`} text={value}  dispatch={this.props.dispatch}/>
        })}
      </div>
    )
  }
}