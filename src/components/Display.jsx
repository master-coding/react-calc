'use strict';

import React, { Component } from 'react'
import { render } from 'react-dom'
import { connect } from 'react-redux'

export default class Display extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    console.log("Display: componentWillMount");

  }
  componentDidMount() {
    console.log("Display: componentDidMount");
  }
  componentDidUpdate() {
    console.log("Display: componentDidUpdate");
  }

  componentWillReceiveProps() {
    console.log("Display: componentWillReceiveProps");
  }

  shouldComponentUpdate() {
    console.log("Display: shouldComponentUpdate.", "return false if you don't want to update UI." );
    return true;
  }

  render() {
    console.log("Display: render");
    return (
      <div>
        <input id="display" type="text" value={this.props.value.result > 0 ? this.props.value.result : this.props.value.expression}/>
      </div>
    )
  }
}
