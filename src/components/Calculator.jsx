'use strict'

import React, { Component } from 'react'
import { render } from 'react-dom'
import { connect } from 'react-redux'
import Display from "./Display.jsx"
import KeyPad from "./KeyPad.jsx"

class Calculator extends Component {
  constructor(props) {
    console.log('App.props = ', props);
    super(props);
  }
  componentDidMount() {
  }
  render() {
    return (
      <div id="calculation-division">
        <Display value={this.props.display}/>
        <KeyPad keys={["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "+", "-", "*", "/", "=", "C"]} dispatch={this.props.dispatch}/>
      </div>
    )
  }
}

function select(store) {
  console.log('calculator state', store);
  return store;
}
export default connect(select)(Calculator)
