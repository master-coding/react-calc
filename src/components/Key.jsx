'use strict';

import React, { Component } from 'react'
import { render } from 'react-dom'
import { connect } from 'react-redux'

export default class Key extends Component {
  constructor(props) {
    super(props);
    this.keyClick = this.keyClick.bind(this);
  }
  keyClick(event){
    console.log("key is pressed", event.target.innerText);
    let key = event.target.innerText;
    if(key === "C"){
      this.props.dispatch({
        "type": "CLEAR_KEY_PRESSED"
      });
    }else if(key === "=") {
      this.props.dispatch({
        "type": "EQUAL_KEY_PRESSED"
      });
    }else {
      this.props.dispatch({
        "type": "KEY_PRESSED",
        "key": key
      });
    }
  }
  render() {
    return (
      <button onClick={this.keyClick} className="key">{this.props.text}</button>
    )
  }
}
