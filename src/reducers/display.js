// export function display(state = {"display":""}, action = {}) {
//     switch (action.type) {
//         case 'KEY_PRESSED':
//             return {"display": state["display"] + action.key};
//         default:
//             return state;
//     }
// }
import * as math from 'mathjs'

export function display(state = { result: 0, expression: '' }, action = {}) {
  switch (action.type) {
    case 'KEY_PRESSED':
      let nextExpression = state.expression + ' ' + action.key
      return { result: 0, expression: nextExpression }
    case 'CLEAR_KEY_PRESSED':
      return { result: 0, expression: '' }
    case 'EQUAL_KEY_PRESSED':
      let result = math.eval(state.expression)
      console.log('result = ', result)
      return { result: result, expression: state.expression }
    default:
      return state
  }
}
