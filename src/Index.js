import { Provider } from 'react-redux'

import appStore from './Reducers'

import ReactDOM from 'react-dom'

import React from 'react'

import { createStore, applyMiddleware } from 'redux'

import thunkMiddleware from 'redux-thunk'

import { Router, Route } from 'react-router'

import History from './History'

import Calculator from './components/Calculator.jsx'
import NameForm from './components/examples/NameForm.jsx';
import './styles/app.scss'
const store = createStore(appStore, applyMiddleware(thunkMiddleware))

ReactDOM.render(
  <Provider store={store}>
    <Router history={History.getHistory()}>
      <Route path="/" component={Calculator} />
    </Router>
  </Provider>,
  document.querySelector('react'),
);
